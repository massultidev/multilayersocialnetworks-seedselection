#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import time
import math
import random
import networkx.algorithms as nxa

from gdev.algorithm.socialnetwork.VoteRank import VoteRank
from gdev.algorithm.socialnetwork.multilayer.diffusion.LinearThresholdModel import LinearThresholdModel
#from gdev.algorithm.evolutional.fitness.LinearThresholdModelFitnessFunction import LinearThresholdModelFitnessFunction
from gdev.algorithm.evolutional.fitness.SocialNetworkLTM import SocialNetworkLTM
from gdev.algorithm.evolutional.Evolutional import Evolutional

class ResearchLab():
    
    ALGORITHM_TYPE_RANDOM = "random"
    ALGORITHM_TYPE_DEGREE_CENTRALITY = "degree-centrality"
    ALGORITHM_TYPE_K_SHELL = "k-shell"
    ALGORITHM_TYPE_VOTE_RANK = "vote-rank"
    ALGORITHM_TYPE_EVOLUTIONAL = "evolutional"
    
    def __init__(self, db):
        self._db = db
        self._stepLimit = 10
        self._seedsPercentage = 0.01
    ##
    
    def _getSeedsCount(self, net):
        return int(math.ceil(net.number_of_nodes()*self._seedsPercentage))
    ##
    
    def runRandom(self, net, testId):
        startTime = time.time()
        
        seeds = random.sample(net.nodes(), self._getSeedsCount(net))
        
        execTime = time.time() - startTime
        
        self._doSummary(net, self.ALGORITHM_TYPE_RANDOM, testId, seeds, execTime, verbose=True)
    ##
    
    def runDegreeCentrality(self, net, testId):
        startTime = time.time()
        
        dCentrNet = nxa.degree_centrality(net)
        dCentrNet = sorted(dCentrNet.items(), key=lambda item: item[1], reverse=True)
        
        seeds = [dCentrNet[idx][0] for idx in range(self._getSeedsCount(net))]
        
        execTime = time.time() - startTime
        
        self._doSummary(net, self.ALGORITHM_TYPE_DEGREE_CENTRALITY, testId, seeds, execTime, verbose=True)
    ##
    
    def runKShell(self, net, testId):
        startTime = time.time()
        
        kShellNet = nxa.core.k_shell(net)
        dCentrNet = nxa.degree_centrality(kShellNet)
        dCentrNet = sorted(dCentrNet.items(), key=lambda item: item[1], reverse=True)
        
        seedsCountAvailable = len(dCentrNet)
        seedsCount = self._getSeedsCount(net)
        
        seedsCountToGetFromKShell = seedsCount
        if seedsCountAvailable < seedsCount:
            seedsCountToGetFromKShell = seedsCountAvailable
        
        seeds = [dCentrNet[idx][0] for idx in range(seedsCountToGetFromKShell)]
        
        if seedsCountAvailable < seedsCount:
            seeds = [dCentrNet[idx][0] for idx in range(seedsCountAvailable)]
            
            dCentrNetF = nxa.degree_centrality(net)
            dCentrNetF = sorted(dCentrNetF.items(), key=lambda item: item[1], reverse=True)
            
            for seedTuple in dCentrNetF:
                seed = seedTuple[0]
                if len(seeds) >= seedsCount:
                    break
                if not seed in seeds:
                    seeds.append(seed)
        
        execTime = time.time() - startTime
        
        self._doSummary(net, self.ALGORITHM_TYPE_K_SHELL, testId, seeds, execTime, verbose=True)
    ##
    
    def runVoteRank(self, net, testId):
        startTime = time.time()
        
        vRank = VoteRank(net, self._getSeedsCount(net), verbose=True)
        vRank.run()
        
        seeds = vRank.getSeeds()
        
        execTime = time.time() - startTime
        
        self._doSummary(net, self.ALGORITHM_TYPE_VOTE_RANK, testId, seeds, execTime, verbose=True)
    ##
    
    def runEvolutional(self, net, testId):
        #net = nxa.core.k_shell(net)
        #fitnessFunction = LinearThresholdModelFitnessFunction(net, stepLimit=10, verbose=False)
        fitnessFunction = SocialNetworkLTM(net)
        
        evo = Evolutional(
            individualGenesCount = self._getSeedsCount(net)
            ,genePool = net.nodes()
            ,populationSize = round(net.number_of_nodes()*0.01)
            ,generationsCount = 100
            ,fitnessFunction = fitnessFunction
            ,crossoverProbability = 0.7
            ,mutationProbability = 0.2
            ,parentPickStrategyClass = None
            ,survivorPickStrategyClass = None
            ,crossoverStrategyClass = None
            ,mutationStrategyClass = None
            ,elitsmRatio = 0.15
            ,preserveBothChildren = False
        )
        execTime = evo.run()
        seeds = evo.getBestSolution().genes
        
        self._doSummary(net, self.ALGORITHM_TYPE_EVOLUTIONAL, testId, seeds, execTime, verbose=False)
    ##
    
    def _doSummary(self, net, name, testId, seeds, execTime, verbose=False):
        print "Calculating coverage..."
        ltm = LinearThresholdModel(net, stepLimit=self._stepLimit, verbose=True, verboseDebug=False)
        startTime = time.time()
        ltm.startDiffusion(seeds)
        print "Coverage calculation time: %s [s]" % (time.time() - startTime)
        
        nodesCountInfected = ltm.getInfectedNodesCount()
        coverage = ltm.getCoverage(withZeroStep=True)
        
        if len(coverage) <= self._stepLimit:
            print "Fixing coverage..."
            lenDifference = self._stepLimit + 1 - len(coverage)
            coverage.extend([coverage[-1]]*lenDifference)
        
        if verbose:
            nodesCountAll = net.number_of_nodes()
            coverageFinal = coverage[-1]
            
            print "Overall calculation time: %s [s]" % execTime
            #print "Best solution is:"
            #pprint(seeds)
            print "Best solution coverage is: %d/%d = %f" % (nodesCountInfected, nodesCountAll, coverageFinal)
        
        self._db.saveResults(name, testId, execTime, coverage)
##
