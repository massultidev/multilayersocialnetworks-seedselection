#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import pandas
import scipy.stats as scs
import sklearn.metrics as sklm
import stac.nonparametric_tests as stacnpt

# from pprint import pprint

from gdev.algorithm.socialnetwork.Tools import Tools
from gdev.utility.Logger import Logger
from gdev.utility.DataVisualizer import DataVisualizer

from app.Database import Database
from app.ResearchLab import ResearchLab

class App():
    
    SPLITTER = "----------------------------------------------------------"
    
    ALGORITHM_TYPE_EVOLUTIONAL = "evolutional"
    ALGORITHM_TYPE_VOTE_RANK = "vote-rank"
    ALGORITHM_TYPE_K_SHELL = "k-shell"
    ALGORITHM_TYPE_DEGREE_CENTRALITY = "degree-centrality"
    ALGORITHM_TYPE_RANDOM = "random"
    
    def __init__(self):
        self._db = Database()
        self._researchLab = ResearchLab(self._db)
        self._netCount = 3
    ##
    
    def parseNetworks(self):
        for idx in range(len(Database.FILENAME_NET_RE_ORIG)):
            print "Converting network:", Database.FILENAME_NET_RE_ORIG[idx]
            
            print "Reading from file..."
            net = self._db.readRealNetwork(idx)
            
            print "Saving network..."
            fileName = self._db.FILENAME_NET_RE % idx
            self._db.dumpNetwork(net, fileName)
    ##
    
    def genNetworks(self):
        nodesPerLayerCount = 500
        layersCount = 3
        edgeProbability = 0.025
        
        for idx in range(self._netCount):
            print "Creating network:", idx
            nodesPerLayerCountCurr = (idx+1)*nodesPerLayerCount
            edgeProbabilityCurr = edgeProbability/(idx+1)
            print "nodesPerLayerCountCurr = %d"   % nodesPerLayerCountCurr
            print "layersCount            = %d"   % layersCount
            print "edgeProbability        = %.12f" % edgeProbabilityCurr
            
            print "Generating network..."
            pymnetNetwork = Tools.genNetwork(
                nodesPerLayerCount = nodesPerLayerCountCurr,
                layersCount = layersCount,
                edgeProbability = edgeProbabilityCurr
            )
            
            print "Converting network..."
            net = Tools.convert(pymnetNetwork)
            
            print "Saving network..."
            fileName = self._db.FILENAME_NET_ER % idx
            self._db.dumpNetwork(net, fileName)
    ##
    
    def _runStudies(self, net, fileName, algorithmType=None):
        print "Running algorithm..."
        if algorithmType == ResearchLab.ALGORITHM_TYPE_EVOLUTIONAL:
            self._researchLab.runEvolutional(net, fileName)
            return
        if algorithmType == ResearchLab.ALGORITHM_TYPE_VOTE_RANK:
            self._researchLab.runVoteRank(net, fileName)
            return
        if algorithmType == ResearchLab.ALGORITHM_TYPE_K_SHELL:
            self._researchLab.runKShell(net, fileName)
            return
        if algorithmType == ResearchLab.ALGORITHM_TYPE_DEGREE_CENTRALITY:
            self._researchLab.runDegreeCentrality(net, fileName)
            return
        if algorithmType == ResearchLab.ALGORITHM_TYPE_RANDOM:
            self._researchLab.runRandom(net, fileName)
            return
        
        raise Exception("Unknown algorithm type provided!")
    ##
    
    def runStudies(self, algorithmType=None):
        if algorithmType is None:
            algorithmType = ResearchLab.ALGORITHM_TYPE_EVOLUTIONAL
        
        for idx in range(self._netCount):
            print self.SPLITTER
            print "Reading network:", idx
             
            fileName = self._db.FILENAME_NET_ER % idx
            net = self._db.loadNetwork(fileName)
             
            self._runStudies(net, fileName, algorithmType)
            
        for idx in range(len(Database.FILENAME_NET_RE_ORIG)):
            print self.SPLITTER
             
            fileName = self._db.FILENAME_NET_RE % idx
             
            print "Reading network:", fileName
            net = self._db.loadNetwork(fileName)
             
            self._runStudies(net, fileName, algorithmType)
    ##
    
    def present(self):
        results = self._db.readResults()
        
        idStudies = results.keys()
        idNetworks = sorted(results[idStudies[0]].keys())
        
        aucs = []
        execTimes = []
        
        for idNetwork in idNetworks:
            coverage = []
            execTime = []
            
            aucForNetwork = []
            
            for idStudy in idStudies:
                coverageForStudy = results[idStudy][idNetwork][Database.FILE_RESULTS_LBL_COVERAGE]
                coverage.append(coverageForStudy)
                execTime.append(results[idStudy][idNetwork][Database.FILE_RESULTS_LBL_EXECTIME])
                
                aucForNetwork.append(sklm.auc(range(len(coverageForStudy)), coverageForStudy))
            
            execTimes.append(execTime)
            
            aucs.append(aucForNetwork)
            
            x = range(len(coverage[0]))
            
            print "NetworkId: ", idNetwork
            DataVisualizer.genPlot(x, coverage, idStudies, xlabel="Kroki", ylabel="Pokrycie")
            
            fName = idNetwork.replace(".json", ".png")
            DataVisualizer.savePlot(fName, verbose=True)
        
        DataVisualizer.genBarChartBroken(execTimes, idStudies, ylabel="Czas [s]", breakPointLower=65, breakPointUpper=70)
        DataVisualizer.savePlot("times.png", verbose=True)
        
        DataVisualizer.genBarChart(aucs, idStudies, ylabel="AUC")
        DataVisualizer.savePlot("aucs.png", verbose=True)
    ##
    
    def _calcChiSquare(self, fValue, *args):
        k = len(args)
        n = len(args[0])
        
        return (fValue*n*(k-1))/float((n-1)+fValue)
    ##
    
    def _calcStatistics(self, data, treatmentLabels, fName):
        import warnings
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            resultFriedman = stacnpt.friedman_test(*data)
        pivots = resultFriedman[3]
        chiSquare = scs.friedmanchisquare(*data)[0]
        print "\nchiSquare: %f\n" % chiSquare
        
        pivotalQuantities = {treatmentLabels[i] : pivots[i] for i in range(len(pivots))}
        resultNemenyi = stacnpt.nemenyi_multitest(pivotalQuantities)
        comparisons = resultNemenyi[0]
        pValues = resultNemenyi[2]
        
        table = {}
        for studyName in treatmentLabels:
            table[studyName] = [""]*len(treatmentLabels)
        for comparisonIdx in range(len(comparisons)):
            x, y = comparisons[comparisonIdx].split(" vs ")
            coordMap = {treatmentLabels[i] : i for i in range(len(treatmentLabels))}
            xIdx = coordMap[x]
            yIdx = coordMap[y]
            
            pValCurr = pValues[comparisonIdx]
            alphas = [0.01, 0.05, 0.001]
            table[y][xIdx] = "%.3f" % pValCurr
            alphaResult = ""
            for alphaIdx in range(len(alphas)):
                alpha = alphas[alphaIdx]
                if pValCurr < alpha:
                    if len(alphaResult):
                        alphaResult += ", "
                    alphaResult += "(%d)" % alphaIdx
            table[x][yIdx] = alphaResult
        
        df = pandas.DataFrame(table, index=treatmentLabels, columns=treatmentLabels)
        for idStudy in treatmentLabels:
            df[idStudy][idStudy] = "-"
        
        print df.to_string(line_width=Logger.getConsoleWidth())
        
        bboxInches = DataVisualizer.genTable(df)
        DataVisualizer.savePlot(fName, bboxInches=bboxInches, verbose=True)
    ##
    
    def statistics(self):
        results = self._db.readResults()
        
        idStudies = results.keys()
        idNetworks = results[idStudies[0]].keys()
        
        resultsFixed = {idNetwork : {idStudy : {} for idStudy in idStudies} for idNetwork in idNetworks}
        
        for idNetwork in idNetworks:
            for idStudy in idStudies:
                coverage = results[idStudy][idNetwork][Database.FILE_RESULTS_LBL_COVERAGE]
                execTime = results[idStudy][idNetwork][Database.FILE_RESULTS_LBL_EXECTIME]
                coverageAUC = sklm.auc(range(len(coverage)), coverage)
                
                resultsFixed[idNetwork][idStudy][Database.FILE_RESULTS_LBL_COVERAGE] = coverageAUC
                resultsFixed[idNetwork][idStudy][Database.FILE_RESULTS_LBL_EXECTIME] = execTime
        
        overallAUCs = []
        overallExecTime = []
        for idStudy in idStudies:
            studyAUC = []
            studyExecTime = []
            
            for idNetwork in idNetworks:
                netAUC = resultsFixed[idNetwork][idStudy][Database.FILE_RESULTS_LBL_COVERAGE]
                studyAUC.append(netAUC)
                netExecTime = resultsFixed[idNetwork][idStudy][Database.FILE_RESULTS_LBL_EXECTIME]
                studyExecTime.append(netExecTime)
            
            overallAUCs.append(studyAUC)
            overallExecTime.append(studyExecTime)
        
        print self.SPLITTER
        print "Statistics for coverage AUC:"
        self._calcStatistics(
            overallAUCs,
            treatmentLabels=idStudies,
            fName="results-auc-table.png"
        )
        print self.SPLITTER
        print "Statistics for exec time:"
        self._calcStatistics(
            overallExecTime,
            treatmentLabels=idStudies,
            fName="results-exectime-table.png"
        )
        
#         chiSquare, pVal = scs.friedmanchisquare(*overallAUCs)
#         print "\nFriedman - SciPy"
#         print "chiSquare: ", chiSquare
#         print "pVal:      ", pVal
#         
#         fValue, pValue, rankings, pivots = stacnpt.friedman_test(*overallAUCs)
#         print "\nFriedman - Stac"
#         print "chiSquare: ", self._calcChiSquare(fValue, *overallAUCs)
#         print "fValue:    ", fValue
#         print "pValue:    ", pValue
#         print "rankings:  ", rankings
#         print "pivots:    ", pivots
#         
#         pivotalQuantities = {idStudies[i] : pivots[i] for i in range(len(pivots))}
#         comparisons, zValues, pValues, pValuesAdjusted = stacnpt.nemenyi_multitest(pivotalQuantities)
#         
#         print "\nNemenyi - Stac"
#         pprint({
#             "comparisons" : comparisons,
#             "zValues" : zValues,
#             "pValues" : pValues,
#             "pValuesAdjusted" : pValuesAdjusted
#         })
#         
#         table = {}
#         for studyName in idStudies:
#             table[studyName] = [""]*len(idStudies)
#         for comparisonIdx in range(len(comparisons)):
#             x, y = comparisons[comparisonIdx].split(" vs ")
#             coordMap = {idStudies[i] : i for i in range(len(idStudies))}
#             xIdx = coordMap[x]
#             yIdx = coordMap[y]
#             
#             pValCurr = pValues[comparisonIdx]
#             alphas = [0.01, 0.05, 0.001]
#             table[y][xIdx] = "%.3f" % pValCurr
#             alphaResult = ""
#             for alphaIdx in range(len(alphas)):
#                 alpha = alphas[alphaIdx]
#                 if pValCurr < alpha:
#                     if len(alphaResult):
#                         alphaResult += ", "
#                     alphaResult += "(%d)" % alphaIdx
#             table[x][yIdx] = alphaResult
#         
#         df = pandas.DataFrame(table, index=idStudies, columns=idStudies)
#         for idStudy in idStudies:
#             df[idStudy][idStudy] = "-"
#         
#         print df
#         
#         bboxInches = DataVisualizer.genTable(df)
#         fName = "results-auc-table.png"
#         DataVisualizer.savePlot(fName, bboxInches=bboxInches, verbose=True)
##
