#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import os
import json
import random

import networkx as nx

from networkx.readwrite import json_graph

from gdev.algorithm.socialnetwork.multilayer.NodeId import NodeId
from gdev.algorithm.socialnetwork.multilayer.diffusion.LinearThresholdModel import LinearThresholdModel
from gdev.utility.Resource import Resource

class Database():
    
    STR_SEPARATOR_LAYER_NODE = "/"
    
    FILENAME_NET_ER = "network-er-%d.json"
    FILENAME_NET_RE = "network-re-%d.json"
    FILENAME_RESULTS = "results.json"
    
    FILE_RESULTS_LBL_EXECTIME = "exec_time"
    FILE_RESULTS_LBL_COVERAGE = "coverage"
    
    FILENAME_NET_RE_ORIG = [
        "network-re-0.edges", # MoscowAthletics2013_multiplex.edges
        "network-re-1.edges"  # NYClimateMarch2014_multiplex.edges
    ]
    
    def saveResults(self, name, testId, execTime, coverage):
        print "Saving results..."
        filePath = Resource.getPath(self.FILENAME_RESULTS)
        
        resultJSON = self.readResults()
        
        if name not in resultJSON.keys():
            resultJSON[name] = {}
        if testId not in resultJSON[name].keys():
            resultJSON[name][testId] = {}
        
        resultJSON[name][testId][self.FILE_RESULTS_LBL_EXECTIME] = execTime
        resultJSON[name][testId][self.FILE_RESULTS_LBL_COVERAGE] = coverage
        
        with open(filePath, 'w') as f:
            f.write(json.dumps(resultJSON, indent=4, separators=(',', ': '), sort_keys=True))
        
        print "Written to a file:"
        print filePath
    ##
    
    def readResults(self):
        print "Reading results..."
        filePath = Resource.getPath(self.FILENAME_RESULTS)
        
        resultJSON = {}
        if os.path.isfile(filePath):
            with open(filePath, 'r') as f:
                resultJSON = json.loads(f.read())
        
        print "Read results from file:"
        print filePath
        
        return resultJSON
    ##
    
    def readRealNetwork(self, netId):
        net = nx.Graph()
        
        net.graph[LinearThresholdModel.LBL_NETWORK_LAYERS] = 3
        
        fileName = self.FILENAME_NET_RE_ORIG[netId]
        filePath = Resource.getPath(fileName)
        
        with open(filePath, 'r') as f:
            for line in f.readlines():
                if line:
                    layerIdStr, nodeId1Str, nodeId2Str, weightStr = line.split()
                    layerId = int(layerIdStr)
                    nodeId1 = long(nodeId1Str)
                    nodeId2 = long(nodeId2Str)
                    weight = float(weightStr)
                    
                    # Adding nodes:
                    nodeId1Obj = NodeId(layerId, nodeId1)
                    attrNode1 = {
                        LinearThresholdModel.LBL_NODE_THRESHOLD : random.random()
                    }
                    net.add_node(nodeId1Obj, attr_dict=attrNode1)
                    nodeId2Obj = NodeId(layerId, nodeId2)
                    attrNode2 = {
                        LinearThresholdModel.LBL_NODE_THRESHOLD : random.random()
                    }
                    net.add_node(nodeId2Obj, attr_dict=attrNode2)
                    
                    # Adding edge:
                    attrDictEdge = {
                        LinearThresholdModel.LBL_EDGE_WEIGHT : weight
                    }
                    net.add_edge(nodeId1Obj, nodeId2Obj, attr_dict=attrDictEdge)
        
        net.remove_edges_from(net.selfloop_edges())
        
        return net
    ##
    
    def dumpNetwork(self, net, fileName):
        labels = {}
        for nodeId in net.nodes():
            labels[nodeId] = "%d%s%d" % (nodeId.layer, self.STR_SEPARATOR_LAYER_NODE, nodeId.node)
        
        net = nx.relabel_nodes(net, labels)
        
        netJSON = json_graph.node_link_data(net)
        
        netString = json.dumps(netJSON)
        
        filePath = Resource.getPath(fileName)
        with open(filePath, 'w') as f:
            f.write(netString)
        
        print "Written to a file:"
        print filePath
    ##
    
    def loadNetwork(self, fileName):
        filePath = Resource.getPath(fileName)
        with open(filePath, 'r') as f:
            netString = f.read()
        
        netJSON = json.loads(netString)
        
        net = json_graph.node_link_graph(netJSON)
        
        labels = {}
        for nodeId in net.nodes():
            layer, node = nodeId.split(self.STR_SEPARATOR_LAYER_NODE, 1)
            labels[nodeId] = NodeId(int(layer), int(node))
        
        net = nx.relabel_nodes(net, labels)
        
        print "Read from a file:"
        print filePath
        
        return net
##
