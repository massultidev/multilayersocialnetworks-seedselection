#!/bin/bash

DIR_SCRIPT=$(dirname $(readlink -f "${0}"))

NAME_ENV_PYTHON=Python2-Masters
NAME_SRC_MAIN=main.py

source activate ${NAME_ENV_PYTHON}
export PYTHONPATH="${DIR_SCRIPT}:${DIR_SCRIPT}/../ext/stac"
python ${DIR_SCRIPT}/${NAME_SRC_MAIN} $@

