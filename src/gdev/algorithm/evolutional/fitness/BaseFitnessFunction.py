#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

class BaseFitnessFunction():
    
    def calcFitness(self, individual):
        individual.fitness = self._calcFitness(individual.genes)
    ##
    
    def _calcFitness(self, genes):
        raise NotImplementedError()
##
