#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import sklearn.metrics as sklm

from gdev.algorithm.evolutional.fitness.BaseFitnessFunction import BaseFitnessFunction
from gdev.algorithm.socialnetwork.multilayer.diffusion.LinearThresholdModel import LinearThresholdModel

class LinearThresholdModelFitnessFunction(BaseFitnessFunction):
    
    def __init__(self, net, stepLimit=None, verbose=False):
        self._model = LinearThresholdModel(net, stepLimit=stepLimit, verbose=verbose)
        self._stepLimit = stepLimit
    ##
    
    def _calcFitness(self, genes):
        self._model.startDiffusion(genes)
        
        #return self._model.getInfectedNodesCount() #FIXME: Old solution end!
        
        coverage = self._model.getCoverage()
        
        if len(coverage) < self._stepLimit:
            lenDifference = self._stepLimit - len(coverage)
            coverage.extend([coverage[-1]]*lenDifference)
        
        return sklm.auc(range(len(coverage)), coverage)
##
