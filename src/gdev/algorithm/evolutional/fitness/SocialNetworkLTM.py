#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

from gdev.algorithm.evolutional.fitness.BaseFitnessFunction import BaseFitnessFunction
from gdev.algorithm.socialnetwork.multilayer.diffusion.LinearThresholdModel import LinearThresholdModel

class SocialNetworkLTM(BaseFitnessFunction):
    
    def __init__(self, net):
        self._net = net
    ##
    
    def _calcFitnessForNode(self, nodeId):
        fitness = 0.0
        for nbr in self._net.neighbors_iter(nodeId):
            threshold = self._net.node[nbr][LinearThresholdModel.LBL_NODE_THRESHOLD]
            fitness += (1.0 - threshold)
        
        return fitness
    ##
    
    def _calcFitness(self, genes):
        fitness = 0.0
        for nodeId in genes:
            fitness += self._calcFitnessForNode(nodeId)
        
        return fitness
##
