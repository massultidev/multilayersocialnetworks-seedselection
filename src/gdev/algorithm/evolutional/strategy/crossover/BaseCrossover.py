#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

class BaseCrossover():
    
    def mate(self, parent1, parent2):
        raise NotImplementedError()
##
