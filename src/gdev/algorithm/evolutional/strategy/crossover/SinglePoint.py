#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import random

from gdev.algorithm.evolutional.Individual import Individual
from gdev.algorithm.evolutional.strategy.crossover.BaseCrossover import BaseCrossover

class SinglePoint(BaseCrossover):
    
    def mate(self, parent1, parent2):
        crossPoint = random.randint(1, len(parent2.genes)-1)
        child1 = Individual(parent1.genes[:crossPoint] + parent2.genes[crossPoint:])
        child2 = Individual(parent2.genes[:crossPoint] + parent1.genes[crossPoint:])
        return (child1, child2)
##
