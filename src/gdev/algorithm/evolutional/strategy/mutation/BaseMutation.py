#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

class BaseMutation():
    
    def __init__(self, genePool):
        self._genePool = genePool
    ##
    
    def mutate(self, individual):
        raise NotImplementedError()
##
