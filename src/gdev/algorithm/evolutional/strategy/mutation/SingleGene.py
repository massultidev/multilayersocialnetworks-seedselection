#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import random

from gdev.algorithm.evolutional.Individual import Individual
from gdev.algorithm.evolutional.strategy.mutation.BaseMutation import BaseMutation

class SingleGene(BaseMutation):
    
    def __init__(self, genePool):
        BaseMutation.__init__(self, genePool)
    ##
    
    def mutate(self, individual):
        mutatePoint = random.randint(0, len(individual.genes)-1)
        geneNew, geneAlternate = random.sample(self._genePool, 2)
        if individual.genes[mutatePoint] == geneNew:
            geneNew = geneAlternate
        return Individual(individual.genes[:mutatePoint] + [geneNew] + individual.genes[(mutatePoint+1):])
##
