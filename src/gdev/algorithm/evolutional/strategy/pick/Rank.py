#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import random

from gdev.algorithm.evolutional.strategy.pick.BasePick import BasePick

class Rank(BasePick):
    
    def __init__(self, individuals):
        BasePick.__init__(self, individuals)
        
        self._individuals.sort(key=lambda x: x.fitness, reverse=True)
        self._individualsRanks = range(len(self._individuals), 0, -1)
        self._individualsRankSum = sum(self._individualsRanks)
    ##
    
    def pick(self):
        selectionThreshold = random.randint(1, self._individualsRankSum)
        currRankSum = 0
        for idx in range(len(self._individuals)):
            currRankSum += self._individualsRanks[idx]
            if currRankSum >= selectionThreshold:
                return self._individuals[idx]
        raise Exception("Could not get individual!")
##
