#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

class BasePick():
    
    def __init__(self, individuals):
        self._individuals = individuals
    ##
    
    def pick(self):
        raise NotImplementedError()
##
        
