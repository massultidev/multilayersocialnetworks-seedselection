#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import math
import time
import random

#from pprint import pprint

from gdev.algorithm.evolutional.Individual import Individual
from gdev.algorithm.evolutional.strategy.pick.Rank import Rank
from gdev.algorithm.evolutional.strategy.crossover.SinglePoint import SinglePoint
from gdev.algorithm.evolutional.strategy.mutation.SingleGene import SingleGene
from gdev.utility.Logger import Logger

class Evolutional():
    
    def __init__(self
            ,individualGenesCount
            ,genePool
            ,populationSize
            ,generationsCount
            ,fitnessFunction
            ,crossoverProbability = 0.7
            ,mutationProbability = 0.2
            ,parentPickStrategyClass = None
            ,survivorPickStrategyClass = None
            ,crossoverStrategyClass = None
            ,mutationStrategyClass = None
            ,elitsmRatio = 0.15
            ,preserveBothChildren = False
        ):
        assert not (len(genePool) < individualGenesCount)
        
        self._individualGenesCount = individualGenesCount
        self._genePool = genePool
        self._populationSize = populationSize
        self._generationsCount = generationsCount
        self._fitnessFunction = fitnessFunction
        self._crossoverProbability = \
            crossoverProbability if crossoverProbability > 0.0 else 0.0
        self._mutationProbability = \
            mutationProbability if mutationProbability > 0.0 else 0.0
        self._parentPickStrategyClass = \
            parentPickStrategyClass if not parentPickStrategyClass is None else Rank
        self._survivorPickStrategyClass = \
            survivorPickStrategyClass if not survivorPickStrategyClass is None else Rank
        self._crossoverStrategy = \
            crossoverStrategyClass() if not crossoverStrategyClass is None else SinglePoint()
        self._mutationStrategy = \
            mutationStrategyClass(self._genePool) if not mutationStrategyClass is None else SingleGene(self._genePool)
        self._eliteIndividualsCount = \
            int(math.floor(float(self._populationSize) * elitsmRatio)) if elitsmRatio > 0.0 else 0
        self._preserveBothChildren = preserveBothChildren
        
        self.clear()
    ##
    
    def run(self):
        print "Starting calculation..."
        generationCalculationTimes = []
        while not self.isFinished():
            self._printProgress()
            
            startTime = time.time()
            
            self.makeGeneration()
            
            execTime = time.time() - startTime
            generationCalculationTimes.append(execTime)
        
        self._printProgress(finish=True)
        
#         bestSolution = self.getBestSolution()
        execTime = sum(generationCalculationTimes)
        
        print "Finished calculation!"
        print "Average generation calculation time: %s [s]" % (execTime/float(len(generationCalculationTimes)))
        print "Overall calculation time: %s [s]" % execTime
#         print "Best solution is:"
#         pprint(bestSolution.genes)
#         print "With fitness value: ", bestSolution.fitness
        
        return execTime
    ##
    
    def _printProgress(self, finish=False):
        percentageValue = math.ceil((float(self.getGenerationNumber())/float(self._generationsCount))*100.0)
        Logger.printProgress(percentageValue, finish)
    ##
    
    def isFinished(self):
        return self._currentGenerationNumber == self._generationsCount
    ##
    
    def getGenerationNumber(self):
        return self._currentGenerationNumber
    ##
    
    def getBestSolution(self):
        self._currentPopulation.sort(key=lambda x: x.fitness, reverse=True)
        return self._currentPopulation[0]
    ##
    
    def clear(self):
        self._currentGenerationNumber = 0
        self._currentPopulation = []
    ##
    
    def makeGeneration(self):
        if self._currentGenerationNumber == 0:
            self._makeRandomPopulation()
        else:
            self._makeNewPopulation()
        
        self._currentGenerationNumber += 1
    ##
    
    def _makeNewPopulation(self):
        offspring = self._makeOffspring()
        
        newPopulation = []
        
        if self._eliteIndividualsCount > 0:
            survivorPickStrategy = self._survivorPickStrategyClass(self._currentPopulation)
            while len(newPopulation) < self._eliteIndividualsCount:
                newPopulation.append(survivorPickStrategy.pick())
        
        survivorPickStrategy = self._survivorPickStrategyClass(offspring)
        while len(newPopulation) < self._populationSize:
            newPopulation.append(survivorPickStrategy.pick())
        
        self._currentPopulation = newPopulation
    ##
    
    def _makeOffspring(self):
        parentPickStrategy = self._parentPickStrategyClass(self._currentPopulation)
        
        offspring = []
        
        individualIndex = -1
        while len(offspring) < self._populationSize:
            individualIndex += 1
            if individualIndex == len(self._currentPopulation):
                individualIndex = 0
            
            individual = self._currentPopulation[individualIndex]
            
            if random.random() < self._crossoverProbability:
                secondParent = parentPickStrategy.pick() # We ignore duplicates here. Should we?
                child1, child2 = self._crossoverStrategy.mate(individual, secondParent)
                self._fitnessFunction.calcFitness(child1)
                self._fitnessFunction.calcFitness(child2)
                if self._preserveBothChildren:
                    offspring.extend([child1, child2])
                else:
                    offspring.append(child1 if child1.fitness >= child2.fitness else child2)
            
            if random.random() < self._mutationProbability:
                child = self._mutationStrategy.mutate(individual)
                self._fitnessFunction.calcFitness(child)
                offspring.append(child)
        
        return offspring
    ##
    
    def _makeRandomPopulation(self):
        while len(self._currentPopulation) < self._populationSize:
            individual = self._genRandomIndividual()
            self._fitnessFunction.calcFitness(individual)
            self._currentPopulation.append(individual)
    ##
    
    def _genRandomIndividual(self):
        individual = Individual([])
        while len(individual.genes) < self._individualGenesCount:
            extractSize = min(self._individualGenesCount - len(individual.genes), len(self._genePool))
            individual.genes.extend(random.sample(self._genePool, extractSize))
        return individual
##
