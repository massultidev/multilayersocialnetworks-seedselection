#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import pymnet as pnet

from gdev.algorithm.socialnetwork.multilayer.diffusion.LinearThresholdModel import LinearThresholdModel

class Tools():
    
    LBL_NETWORK_LAYERS = "layers"
    LBL_NODE_THRESHOLD = "threshold"
    LBL_EDGE_WEIGHT = "weight"
    
    @staticmethod
    def genNetwork(
            nodesPerLayerCount = 5
            ,layersCount = 2
            ,edgeProbability = 0.2
        ):
        return pnet.models.er_multilayer(nodesPerLayerCount, layersCount, edgeProbability, True)
    ##
    
    @classmethod
    def convert(cls, pymnetNetwork, model=LinearThresholdModel, verbose=False):
        if model == LinearThresholdModel:
            return LinearThresholdModel.preprocess(pymnetNetwork, verbose=verbose)
        
        raise Exception("Unknown model!")
##
