#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import random

import networkx as nx

from gdev.algorithm.socialnetwork.multilayer.NodeId import NodeId

class LinearThresholdModel():
    
    LBL_NETWORK_LAYERS = "layers"
    LBL_NODE_THRESHOLD = "threshold"
    LBL_EDGE_WEIGHT = "weight"
    
    def __init__(self, net, stepLimit=None, verbose=False, verboseDebug=False):
        self._net = net
        self._diffusionSteps = None
        self._stepCurrent = None
        self._stepLimit = stepLimit
        self._verbose = verbose
        self._verboseDebug = verboseDebug
        
        self._clean()
    ##
    
    def _clean(self):
        self._diffusionSteps = {}
        self._stepCurrent = 0
    ##
    
    @classmethod
    def preprocess(cls, pymnetNetwork, verbose=False):
        net = nx.Graph()
        
        layers = len(pymnetNetwork.get_layers())
        nodes = len(list(pymnetNetwork))
        
        if verbose:
            print "Layers: ", layers
            print "Nodes: ", nodes * layers
            print
        
        net.graph[cls.LBL_NETWORK_LAYERS] = layers
        
        for idxLayer in range(layers):
            for idxNode in range(nodes):
                
                if verbose:
                    print "[L/N]:[%d/%d] Neighbors: " % (idxLayer, idxNode), list(pymnetNetwork[idxNode, idxLayer])
                
                # Adding node:
                nodeId = NodeId(idxLayer, idxNode)
                attrNode = {
                    cls.LBL_NODE_THRESHOLD : random.random()
                }
                net.add_node(nodeId, attr_dict=attrNode)
                
                for neighbor in list(pymnetNetwork[idxNode, idxLayer]):
                    
                    # Adding edge:
                    nbrLayer = neighbor[1]
                    nbrNode = neighbor[0]
                    edgeWeight = pymnetNetwork[idxNode, nbrNode, idxLayer, nbrLayer]
                    nbrId = NodeId(nbrLayer, nbrNode)
                    attrDictEdge = {
                        cls.LBL_EDGE_WEIGHT : edgeWeight
                    }
                    net.add_edge(nodeId, nbrId, attr_dict=attrDictEdge)
                    
                    if verbose:
                        print "\t(L,N): (%d,%d)-(%d,%d) = %f" % (idxLayer, idxNode, nbrLayer, nbrNode, edgeWeight)
                        print "\tAdding edge: (%s:%s)" %(nodeId.toTuple(), nbrId.toTuple())
        
        return net
    ##
    
    def getCoverage(self, withZeroStep=False):
        nodeCount = self._net.number_of_nodes()
        
        coverage = []
        infectedCount = 0
        for diffusionStep in self._diffusionSteps.values():
            infectedCount += len(diffusionStep)
            coverage.append(infectedCount/float(nodeCount))
        
        if not withZeroStep:
            coverage.pop(0)
        
        return coverage
    ##
    
    def getInfectedNodesCount(self):
        infectedCount = 0
        
        for diffusionStep in self._diffusionSteps.values():
            infectedCount += len(diffusionStep)
        
        return infectedCount
    ##
    
    def startDiffusion(self, seeds):
        self._clean()
        
        for seed in seeds:
            self._infectNode(seed)
    
        if self._verboseDebug:
            print "Infected in step %d:" % self._stepCurrent
            print self._diffusionSteps[self._stepCurrent]
        
        while not self._isDiffusionFinished():
            
            if self._verbose:
                print "Run step: %d [Already infected: %.12f]" \
                    % ((self._stepCurrent+1), self.getCoverage(withZeroStep=True)[-1])
            self._runStep()
            
            if self._verboseDebug:
                print "Infected in step %d:" % self._stepCurrent
                print self._diffusionSteps[self._stepCurrent]
    ##
    
    def _runStep(self):
        self._stepCurrent += 1
        
        for infectedNode in self._getInfectedNodes(fromPrevStep=True):
            
            if self._verboseDebug:
                print "\tTrying to infect neighbors of node: ", infectedNode
                print "\tNeighbors: ", self._net.neighbors_iter(infectedNode)
            
            self._sweepNode(infectedNode)
    ##
    
    def _sweepNode(self, nodeId):
        for nbr in self._net.neighbors_iter(nodeId):
            if not self._isInfected(nbr):
                self._tryToInfect(nbr)
    ##
    
    def _tryToInfect(self, nodeId):
        if self._verboseDebug:
            print "\tTrying to infect node: ", nodeId
            print "\tCurrently infected: ", self._getInfectedNodes()
            print "\t\tNode %s neighbors: " % nodeId, self._net.neighbors_iter(nodeId)
        
        nbrEdgeWeights = self._getNeighborsEdgeWeights(nodeId)
        nbrEdgeWeights = self._normalizeWeights(nbrEdgeWeights)
        
        influence = self._calcInfluence(nbrEdgeWeights)
        
        infectionThreshold = self._getThreshold(nodeId)
        
        if self._verboseDebug:
            print "\t\t\tNode %s influence/threshold: %f/%f" % (nodeId, influence, infectionThreshold)
        
        if influence >= infectionThreshold:
            self._infectNode(nodeId)
    ##
    
    def _getNeighborsEdgeWeights(self, nodeId):
        nbrEdgeWeights = {}
        for nbr in self._net.neighbors_iter(nodeId):
            nbrEdgeWeight = self._net.edge[nodeId][nbr][self.LBL_EDGE_WEIGHT]
            nbrEdgeWeights[nbr] = nbrEdgeWeight
            
            if self._verboseDebug:
                print "\t\t\tEdge (%s:%s) weight: %f" % (nodeId, nbr, nbrEdgeWeight)
        
        return nbrEdgeWeights
    ##
    
    def _normalizeWeights(self, weights):
        weightSum = sum(weights.values())
        if weightSum > 1.0:
            for nbr in weights.keys():
                weight = weights[nbr]
                weights[nbr] = weight/weightSum
        
        return weights
    ##
    
    def _calcInfluence(self, weights):
        influence = 0.0
        for nbr, weight in weights.items():
            if self._isInfected(nbr):
                influence += weight
        
        return influence
    ##
    
    def _getThreshold(self, nodeId):
        return self._net.node[nodeId][self.LBL_NODE_THRESHOLD]
    ##
    
    def _isDiffusionFinished(self):
        if not self._stepCurrent in self._diffusionSteps.keys():
            return True
        if len(self._diffusionSteps[self._stepCurrent]) == 0:
            return True
        if self._stepCurrent == self._stepLimit:
            return True
        
        return False
    ##
    
    def _getInfectedNodes(self, fromPrevStep=False):
        if fromPrevStep:
            return self._diffusionSteps[self._stepCurrent - 1]
        
        infectedNodes = []
        for diffusionStepIdx, diffusionStepInfectedNodes in self._diffusionSteps.items():
            if diffusionStepIdx >= self._stepCurrent:
                break
            infectedNodes.extend(diffusionStepInfectedNodes)
        return infectedNodes
    ##
    
    def _isInfected(self, nodeId):
        for diffusionStep in self._diffusionSteps.values():
            if nodeId in diffusionStep:
                return True
        
        return False
    ##
    
    def _infectNode(self, nodeId):
        if self._stepCurrent not in self._diffusionSteps.keys():
            self._diffusionSteps[self._stepCurrent] = set()
        
        for layer in range(self._net.graph[self.LBL_NETWORK_LAYERS]):
            nodeIdToInfect = NodeId(layer, nodeId.node)
            if self._net.has_node(nodeIdToInfect):
                self._diffusionSteps[self._stepCurrent].add(nodeIdToInfect)
##
