#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

class NodeId():
    
    def __init__(self, layer, node):
        self.layer = layer
        self.node = node
    ##
    
    def __ne__(self, other):
        return not self.__eq__(other)
    def __eq__(self, other):
        return ((self is not None) and
                (other is not None) and
                (self.layer == other.layer) and
                (self.node == other.node))
    ##
    
    def __hash__(self):
        return hash(repr(self))
    ##
    
    def __repr__(self):
        return self.__str__()
    def __str__(self):
        return self.toString()
    ##
    
    def toString(self):
        return "<L/N : %d/%d>" % (self.layer, self.node)
    def toTuple(self):
        return (self.layer, self.node)
##
