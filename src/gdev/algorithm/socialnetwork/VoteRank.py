#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import math

from gdev.utility.Logger import Logger

class VoteRank():
    
    _LBL_VOTERANK = "voterank"
    
    _LBL_STRENGTH = "strength"
    _LBL_VOTES = "votes"
    
    def __init__(self, net, seedCount, verbose=False):
        self._net = None
        self._netBack = net
        self._seedCount = seedCount
        self._verbose = verbose
        self._seeds = None
        self._avgDeg = None
    ##
    
    def getSeeds(self):
        return self._seeds
    ##
    
    def run(self):
        self._clean()
        
        while not self._isFinished():
            self._vote()
            self._selectSeed()
    ##
    
    def _clean(self):
        self._net = self._netBack.copy()
        self._seeds = []
        self._avgDeg = self._getAvgDeg()
        
        for node in self._net.nodes_iter():
            self._net.node[node][self._LBL_VOTERANK] = {
                self._LBL_STRENGTH : 1.0,
                self._LBL_VOTES : 0.0
            }
    ##
    
    def _getAvgDeg(self):
        return sum(self._net.degree().values()) / float(self._net.number_of_nodes())
    ##
    
    def _vote(self):
        for node in self._net.nodes_iter():
            for nbr in self._net.neighbors_iter(node):
                self._net.node[nbr][self._LBL_VOTERANK][self._LBL_VOTES] += \
                    self._net.node[node][self._LBL_VOTERANK][self._LBL_STRENGTH]
    ##
    
    def _selectSeed(self):
        nodeValueTuple = max(
            self._net.nodes_iter(data=True),
            key=lambda x: x[1][self._LBL_VOTERANK][self._LBL_VOTES]
        )
        node = nodeValueTuple[0]
        self._seeds.append(node)
        
        self._updateVoteStrengths(node)
        self._weakenSeed(node)
    ##
    
    def _weakenSeed(self, node):
        self._net.remove_node(node)
    ##
    
    def _updateVoteStrengths(self, node):
        for nbr in self._net.neighbors_iter(node):
            self._net.node[nbr][self._LBL_VOTERANK][self._LBL_STRENGTH] -= 1.0 / self._avgDeg
    ##
    
    def _isFinished(self):
        seedCountCurrent = len(self._seeds)
        
        if self._verbose:
            percentageValue = math.ceil((float(seedCountCurrent)/float(self._seedCount))*100.0)
            finish = False if seedCountCurrent < self._seedCount else True
            Logger.printProgress(percentageValue, finish=finish)
        
        return (seedCountCurrent >= self._seedCount)
##
