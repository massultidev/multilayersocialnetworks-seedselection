#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import os
import sys
import math

class Logger():
    
    @staticmethod
    def getConsoleDimensions():
        h, w = os.popen('stty size', 'r').read().split()
        return (int(h), int(w))
    ##
    
    @classmethod
    def getConsoleHeight(cls):
        return cls.getConsoleDimensions()[0]
    ##
    
    @classmethod
    def getConsoleWidth(cls):
        return cls.getConsoleDimensions()[1]
    ##
    
    @classmethod
    def printProgress(cls, percentageValue, finish=False):
        consoleWidth = cls.getConsoleWidth()-7
        barResolutionRatio = float(consoleWidth)/100.0
        fillLength = int(math.ceil(float(percentageValue) * barResolutionRatio))
        
        sys.stdout.write(("\r[%-"+str(consoleWidth)+"s] %d%%") % ('='*fillLength, percentageValue))
        if finish:
            sys.stdout.write("\n")
        sys.stdout.flush()
##
