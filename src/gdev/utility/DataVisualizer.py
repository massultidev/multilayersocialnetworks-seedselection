#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from gdev.utility.Resource import Resource

class DataVisualizer():
    
    #pnet.draw(pymnetNetwork, show=True)

    @staticmethod
    def genPlot(x, ySeries, labels, xlabel=None, ylabel=None):
        xlimOffset = 0.1
        ylimOffset = 0.02
        plt.xlim(min(x)-xlimOffset, max(x)+xlimOffset)
        plt.ylim(0.0-ylimOffset, 1.0+ylimOffset)
        plt.grid(True)
        for idx in range(len(ySeries)):
            plt.plot(x, ySeries[idx], '-X', label=labels[idx])
        
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend(loc='best', ncol=1)
    ##
    
    @classmethod
    def _genBarChartOnAx(cls, ax, ySeries, labels, barWidth=0.16, ylabel=None, isBroken=False):
        N = len(ySeries)
        ind = np.arange(N) # The x locations for the groups.
        
        rects = []
        for idx in range(len(ySeries)):
            ind = ind + barWidth
            rects.append(ax.bar(ind, ySeries[idx], barWidth))
        
        plt.axis('tight')
        ax.set_xticks(np.arange(N) + barWidth*len(ySeries)/2 + barWidth)
        ax.set_xticklabels([])
        if not ylabel is None:
            plt.ylabel(ylabel)
        
        posYLegend = 1.12 if not isBroken else 1.22
        ax.legend(
            [rect[0] for rect in rects], labels,
            loc='upper center', bbox_to_anchor=(0.5, posYLegend), ncol=len(ySeries)
        )
    ##
    
    @classmethod
    def genBarChartBroken(cls, ySeries, labels, barWidth=0.16, ylabel=None, breakPointLower=None, breakPointUpper=None):
        dataFixed = list(zip(*ySeries))
        
        (ax1, ax2) = plt.subplots(2, 1, sharex=True)[1]
        
        for ax in (ax1, ax2):
            cls._genBarChartOnAx(ax, dataFixed, labels, barWidth, ylabel, isBroken=True)
        
        #TODO: Make it dynamic!
        ax1.set_ylim(breakPointUpper, ax1.get_ylim()[1]) # outliers only
        ax2.set_ylim(ax2.get_ylim()[0], breakPointLower) # most of the data
        
        ax1.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        
        ax1.xaxis.tick_top()
        ax1.tick_params(labeltop='off')
        ax2.xaxis.tick_bottom()
        ax2.legend_.remove()
        
        d = .015
        kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False)
        
        ax1.plot((-d, +d), (-d, +d), **kwargs)       # top-left diagonal
        ax1.plot((1 - d, 1 + d), (-d, +d), **kwargs) # top-right diagonal
        
        kwargs.update(transform=ax2.transAxes)
        
        ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)       # bottom-left diagonal
        ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs) # bottom-right diagonal
    ##
    
    @classmethod
    def genBarChart(cls, ySeries, labels, barWidth=0.16, ylabel=None):
        dataFixed = list(zip(*ySeries))
         
        ax = plt.subplot(111)
        
        cls._genBarChartOnAx(ax, dataFixed, labels, barWidth, ylabel)
    ##
    
    @staticmethod
    def genTable(pandasDataFrame, offValue=None, offTransparency=True):
        if offValue is None:
            offValue = 10
        
        fig, ax = plt.subplots()
        
        if offTransparency:
            fig.patch.set_visible(False)
        ax.axis('off')
        ax.axis('tight')
        
        table = ax.table(
            cellText=pandasDataFrame.values,
            rowLabels=pandasDataFrame.index,
            colLabels=pandasDataFrame.columns,
            cellLoc='right',
            rowLoc='left',
            colLoc='left',
            loc='center'
        )
        
        fig.tight_layout()
        plt.gcf().canvas.draw()
        
        # Get bounding box of table:
        points = table.get_window_extent(plt.gcf()._cachedRenderer).get_points()
        
        # Add 10 pixel spacing:
        points[0,:] -= offValue
        points[1,:] += offValue
        points[0,0] += offValue + 30
        
        # Get new bounding box in inches:
        bboxInches = matplotlib.transforms.Bbox.from_extents(points/plt.gcf().dpi)
        
        return bboxInches
    ##
    
    @staticmethod
    def savePlot(fName, bboxInches=None, verbose=False):
        if bboxInches is None:
            bboxInches = 'tight'
        
        fPath = Resource.getPath(fName)
        plt.savefig(fPath, bbox_inches=bboxInches)
        
        plt.gcf().clear()
        
        if verbose:
            print "Saved plot to file:"
            print fPath
##
