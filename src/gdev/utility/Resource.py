#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import os
import sys

class Resource():
    
    @classmethod
    def getPath(cls, name):
        resourcePath = "../data/R"
        return os.path.join(
            os.path.abspath(os.path.join(os.path.dirname(sys.modules["__main__"].__file__), resourcePath)),
            name
        )
##
