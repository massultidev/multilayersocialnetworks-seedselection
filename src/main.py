#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: Paweł Grochowski
# @Year: 2017
#

import argparse

from app.App import App
from app.ResearchLab import ResearchLab

# Global defines:
APP_NAME = "seedSelector"
APP_VER = "0.3"
DESCRIPTION = "Application for testing different methods of initial seed selection in multilayer social networks."

def main():
    # Define different argument constants:
    CMD_ARG_PARSE = "parse"
    CMD_ARG_GEN = "gen"
    CMD_ARG_EVOLUTIONAL = ResearchLab.ALGORITHM_TYPE_EVOLUTIONAL
    CMD_ARG_VOTE_RANK = ResearchLab.ALGORITHM_TYPE_VOTE_RANK
    CMD_ARG_K_SHELL = ResearchLab.ALGORITHM_TYPE_K_SHELL
    CMD_ARG_DEGREE_CENTRALITY = ResearchLab.ALGORITHM_TYPE_DEGREE_CENTRALITY
    CMD_ARG_RANDOM = ResearchLab.ALGORITHM_TYPE_RANDOM
    CMD_ARG_PRESENT = "present"
    CMD_ARG_STAT = "stat"
    
    HELP_PARSE = "Parse real data networks."
    HELP_GEN = "Generate Erdo-Renyi multilayer networks."
    HELP_EVOLUTIONAL = "Get results for Evolutional seed selection algorithm."
    HELP_VOTE_RANK = "Get results for VoteRank seed selection algorithm."
    HELP_K_SHELL = "Get results for K-Shell seed selection algorithm."
    HELP_DEGREE_CENTRALITY = "Get results for DegreeCentrality seed selection algorithm."
    HELP_RANDOM = "Get results for Random seed selection."
    HELP_PRESENT = "Visualize results."
    HELP_STAT = "Calculate statistics of previously obtained results."
    
    VAL_DEF_PARSE = False
    VAL_DEF_GEN = False
    VAL_DEF_EVOLUTIONAL = False
    VAL_DEF_VOTE_RANK = False
    VAL_DEF_K_SHELL = False
    VAL_DEF_DEGREE_CENTRALITY = False
    VAL_DEF_RANDOM = False
    VAL_DEF_PRESENT = False
    VAL_DEF_STAT = False
    
    # Create arguments parser instance:
    cmdArgParser = argparse.ArgumentParser(
        prog=APP_NAME,
        version=APP_VER,
        description=DESCRIPTION
    )
    
    # Add arguments:
    cmdArgExclusiveGroup = cmdArgParser.add_mutually_exclusive_group()
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_PARSE, "-"+CMD_ARG_PARSE[0].upper(), help=HELP_PARSE,
        action="store_true", dest=CMD_ARG_PARSE, default=VAL_DEF_PARSE
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_GEN, "-"+CMD_ARG_GEN[0].upper(), help=HELP_GEN,
        action="store_true", dest=CMD_ARG_GEN, default=VAL_DEF_GEN
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_EVOLUTIONAL, help=HELP_EVOLUTIONAL,
        action="store_true", dest=CMD_ARG_EVOLUTIONAL, default=VAL_DEF_EVOLUTIONAL
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_VOTE_RANK, help=HELP_VOTE_RANK,
        action="store_true", dest=CMD_ARG_VOTE_RANK, default=VAL_DEF_VOTE_RANK
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_K_SHELL, help=HELP_K_SHELL,
        action="store_true", dest=CMD_ARG_K_SHELL, default=VAL_DEF_K_SHELL
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_DEGREE_CENTRALITY, help=HELP_DEGREE_CENTRALITY,
        action="store_true", dest=CMD_ARG_DEGREE_CENTRALITY, default=VAL_DEF_DEGREE_CENTRALITY
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_RANDOM, help=HELP_RANDOM,
        action="store_true", dest=CMD_ARG_RANDOM, default=VAL_DEF_RANDOM
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_PRESENT, help=HELP_PRESENT,
        action="store_true", dest=CMD_ARG_PRESENT, default=VAL_DEF_PRESENT
    )
    cmdArgExclusiveGroup.add_argument(
        "--"+CMD_ARG_STAT, help=HELP_STAT,
        action="store_true", dest=CMD_ARG_STAT, default=VAL_DEF_STAT
    )
    
    args = cmdArgParser.parse_args()
    
    stateParse = getattr(args, CMD_ARG_PARSE)
    stateGen = getattr(args, CMD_ARG_GEN)
    stateEvolutional = getattr(args, CMD_ARG_EVOLUTIONAL)
    stateVoteRank = getattr(args, CMD_ARG_VOTE_RANK)
    stateKShell = getattr(args, CMD_ARG_K_SHELL)
    stateDegreeCentrality = getattr(args, CMD_ARG_DEGREE_CENTRALITY)
    stateRandom = getattr(args, CMD_ARG_RANDOM)
    statePresent = getattr(args, CMD_ARG_PRESENT)
    stateStat = getattr(args, CMD_ARG_STAT)
    
    app = App()
    
    if stateParse:
        return app.parseNetworks()
    if stateGen:
        return app.genNetworks()
    if stateEvolutional:
        return app.runStudies(ResearchLab.ALGORITHM_TYPE_EVOLUTIONAL)
    if stateVoteRank:
        return app.runStudies(ResearchLab.ALGORITHM_TYPE_VOTE_RANK)
    if stateKShell:
        return app.runStudies(ResearchLab.ALGORITHM_TYPE_K_SHELL)
    if stateDegreeCentrality:
        return app.runStudies(ResearchLab.ALGORITHM_TYPE_DEGREE_CENTRALITY)
    if stateRandom:
        return app.runStudies(ResearchLab.ALGORITHM_TYPE_RANDOM)
    if statePresent:
        return app.present()
    if stateStat:
        return app.statistics()
    
    cmdArgParser.print_help()
##

if __name__ == "__main__":
    main()
##

